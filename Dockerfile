FROM hivemq/hivemq4:latest
RUN rm /opt/hivemq-4.7.6/extensions/hivemq-kafka-extension/DISABLED
COPY kafka-configuration.xml /opt/hivemq-4.7.6/extensions/hivemq-kafka-extension/kafka-configuration.xml